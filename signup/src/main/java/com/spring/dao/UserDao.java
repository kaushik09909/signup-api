package com.spring.dao;

import java.util.List;

import com.spring.beans.User;

public interface UserDao {
	
	void saveAll(User user);
	public User findByEmail(String Id);
	public List<User> getAllUser();
}
