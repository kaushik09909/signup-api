package com.spring.dao.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.beans.User;
import com.spring.dao.UserDao;
import com.spring.repository.UserRepository;

@Repository("UserDao")
public class UserDaoImpl implements UserDao{

	@Autowired
	private UserRepository userRepository;
	
	public void saveAll(User user) {
		userRepository.save(user);
	}
	public User findByEmail(String Id) {
		User user = userRepository.findByEmail(Id);
		return user;
	}
	public List<User> getAllUser(){
		return userRepository.findAll();
	}
}
