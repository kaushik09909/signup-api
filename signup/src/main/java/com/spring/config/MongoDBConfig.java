package com.spring.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.spring.beans.Course;
import com.spring.repository.CourseRepository;

@EnableMongoRepositories(basePackageClasses = CourseRepository.class)
@Configuration("CourseDao")
public class MongoDBConfig {

	@Autowired
	CourseRepository courseRepository;
	@Bean(name="Course")
	CommandLineRunner commandLineRunner(CourseRepository courseRepository) {
		return strings-> {
			courseRepository.save(new Course("Java","Y"));
			courseRepository.save(new Course("Python","Y"));
			courseRepository.save(new Course("C","Y"));
			courseRepository.save(new Course("C++", "Y"));
			courseRepository.save(new Course("Ruby", "Y"));
			courseRepository.save(new Course("Pascal", "Y"));
			courseRepository.save(new Course(".Net", "Y"));
			courseRepository.save(new Course("Oracle", "Y"));
			courseRepository.save(new Course("Spring", "Y"));
			};
		}
	
	public List<Course> getAllCourses(){
		return courseRepository.findAll();
	}
	}
