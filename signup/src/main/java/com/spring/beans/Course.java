package com.spring.beans;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="course")
public class Course implements Serializable{ 
	 
	private static final long serialVersionUID = 1L;
	@Id
	private String skill;
	private String status;
	
	public Course(String skill, String status) {
		super();
		this.skill = skill;
		this.status = status;
	}
	public Course() {
		super();
	}
	public String getSkill() {
		return this.skill;
	}
	public void setSkill(String skill) {
		this.skill = skill;
	}
	public String getStatus() {
		return this.status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
