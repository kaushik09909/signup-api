package com.spring.beans;


import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

//import javax.persistence.*;


@Document(collection="user")
public class User implements Serializable {
	private static final long serialVersionUID = 1L; 
	
	/*@Column(name = "USER_ID", unique = true, nullable = false)*/
	@Id
	private String email;
	//@Column(name = "NAME", nullable=false)
	private String name;
	//@Column(name= "PASSWORD", nullable=false)
	private String password;
	//courses user courses
	private String courses[]=new String[5];
	
	
	
	public String[] getCourses() {
		return courses;
	}
	public void setCourses(String[] courses) {
		this.courses = courses;
	}
	public String getemail() {
		return email;
	}
	public void setemail(String email) {
		this.email = email;
	}
	public String getname() {
		return name;
	}
	public void setname(String name) {
		this.name = name;
	}
	public String getpassword() {
		return password;
	}
	public void setpassword(String password) {
		this.password = password;
	}
	
	@Override
	public String toString() {
		return this.email;
	}
	
	

}
