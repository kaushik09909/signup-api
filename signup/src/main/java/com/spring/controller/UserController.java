package com.spring.controller;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.spring.beans.Course;
import com.spring.beans.User;
import com.spring.config.MongoDBConfig;
import com.spring.service.UserService;




@RestController
@RequestMapping(value="/api")
public class UserController {
	
	@Autowired
	MongoDBConfig mongo;
	@Autowired
	UserService userService;
	/*@Autowired
	CourseService courseService;*/
	
	@RequestMapping(value="/signup" , method=RequestMethod.POST)
    @CrossOrigin(origins="http://localhost:3000/Signup")
    @Consumes(MediaType.APPLICATION_JSON)
	@ResponseBody
    public String createUser(@RequestBody String json) throws JsonProcessingException {
		User user = new User();
		List<Course> courses= mongo.getAllCourses();
		String exists="false"; 
    	 ObjectMapper mapper=new ObjectMapper();
    	try {
			user=mapper.readValue(json, User.class);
			List<User> user1 = userService.getAllUser();
			for(User u:user1) {
				if(u.getemail().toLowerCase().equals(user.getemail().toLowerCase())) {
					exists="true";
					break;
				}
			}
			if(exists=="true") {
				return exists;
			}
			else {
				userService.saveAll(user);
			}
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	 
    	 return mapper.writeValueAsString(courses);
    }
	
	
	@RequestMapping(value="/login" , method=RequestMethod.POST)
    @CrossOrigin(origins="http://localhost:3000/login")
    @Consumes(MediaType.APPLICATION_JSON)
	@ResponseBody
    public String validateUser(@RequestBody String json) throws JsonProcessingException {
		User user = new User(); 
    	 ObjectMapper mapper=new ObjectMapper();
    	 String exists="false";
    	try {
			user=mapper.readValue(json, User.class);
			List<User> user1 = userService.getAllUser();
			for(User u:user1) {
				if(u.getemail().toLowerCase().equals(user.getemail().toLowerCase())&&u.getpassword().equals(user.getpassword())) {
					User user3=u;
					return mapper.writeValueAsString(user3);
					
				}
				else if(u.getemail().toLowerCase().equals(user.getemail().toLowerCase())&& !(u.getpassword().equals(user.getpassword()))) {
					exists="true";
					return exists;
				}
			}
			
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return exists;
    }
	
	
	@RequestMapping(value="/preference" , method=RequestMethod.POST)
    @CrossOrigin(origins="http://localhost:3000/login")
    @Consumes(MediaType.APPLICATION_JSON)
	@ResponseBody
    public String updateUser(@RequestBody String json) throws JsonProcessingException {
		User user = new User(); 
    	 ObjectMapper mapper=new ObjectMapper();
    	 String exists="false";
    	try {
			user=mapper.readValue(json, User.class);
			User userDetails=userService.getUserByEmail(user.getemail());
			userDetails.setCourses(user.getCourses());
			userService.saveAll(userDetails);
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return exists;
    }
}
