package com.spring.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.spring.beans.Course;

public interface CourseRepository extends MongoRepository<Course, String> {

}
