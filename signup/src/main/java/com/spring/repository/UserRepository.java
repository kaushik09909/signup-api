package com.spring.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.spring.beans.User;

public interface UserRepository extends MongoRepository<User, String>{

	public User findByEmail(String Id);

 
}
