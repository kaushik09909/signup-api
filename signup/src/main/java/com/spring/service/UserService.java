package com.spring.service;

import java.util.List;

import com.spring.beans.User;


public interface UserService {
	
	public void saveAll(User user);
	public User findById(String id);
	public List<User> getAllUser();
	public User getUserByEmail(String Id);
}
