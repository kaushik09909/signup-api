package com.spring.service.Imp;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.beans.User;
import com.spring.dao.UserDao;
import com.spring.service.UserService;

@Service("UserService")

public class UserServiceImp implements UserService{
	
	@Autowired
	private UserDao dao;
	
	public void saveAll(User user){
		dao.saveAll(user);
	}
	public User findById(String id) {
		return dao.findByEmail(id);
	}
	public List<User> getAllUser(){
		return dao.getAllUser();
	}
	public User getUserByEmail(String Id) {
		return dao.findByEmail(Id);
	}
}
